zeptodb
=======

What is it?
-----------

zeptodb is a small collection of relatively tiny command-line tools
for interacting with DBM databases.  For the uninitiated, DBM
databases are flat (non-relational) a databases; in other words, they
are persistent key-value hash tables. Typically they are created via a
library for C, Python, Perl, etc. These tools fill in a gap by
providing useful command-line tools. Some DBM libraries come with
really basic binaries for manipulating the databases, but they are not
designed to be very flexible.

These tools may be helpful in scripts, for example, when persistant
data storage is needed but when a full database would be overkill.
They may also be useful if, for whatever reason, one would like to
manipulate, via the command-line or scripts, DBM databases created by
other programs.

Back-ends
---------

By default, zeptodb uses the
[GNU dbm](http://www.gnu.org/software/gdb) (GDBM) library to create
and manipulate the dbm databases.  Alternatively, you may instead
choose to use the [Kyoto Cabinet](http://fallabs.com/kyotocabinet/)
library instead.  This is specified by passing the
`--with-kyotocabinet` option to the configure script before compiling
zeptodb.

Note that databases created with these two different back-ends are
*not* compatible, thus databases created with Kyoto Cabinet can
only be accessed by zeptodb if it has been compiled with support for
the library.

Databases created with Kyoto Cabinet are required to have the
`.kch` file extension.  By convention, databases created with
GDBM should have the `.db` file extension.

For most purposes, databases created with GDBM should be sufficient.
For particularly large data sets, however, Kyoto Cabinet is
preferred, since it can add values more quickly and has a much larger
upper limit on the database size.  On the other hand, Kyoto Cabinet is
not as widely available in GNU/Linux distributions as GDBM so it often
must be installed manually.

Contributing
------------

Patches are welcome!

Author
------

Brandon Invergo <brandon@invergo.net>

License
-------

zeptodb is free software: you can redistribute it and/or modify it
under the terms of the GNU General Public License as published by the
Free Software Foundation, either version 3 of the License, or (at your
option) any later version.
       
zeptodb is distributed in the hope that it will be useful, but WITHOUT
ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License
for more details.

You should have received a copy of the GNU General Public License
along with zeptodb.  If not, see <http://www.gnu.org/licenses/>.
