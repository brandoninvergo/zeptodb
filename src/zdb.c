/* zdb.c

   Copyright © 2013, 2015 Brandon Invergo <brandon@invergo.net>

   This file is part of zeptodb

   zeptodb is free software: you can redistribute it and/or modify
   it under the terms of the GNU General Public License as published by
   the Free Software Foundation, either version 3 of the License, or
   (at your option) any later version.

   zeptodb is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.

   You should have received a copy of the GNU General Public License
   along with zeptodb.  If not, see <http://www.gnu.org/licenses/>.
*/

#include "zdb.h"

void *
zdb_create (char *dbfile, size_t mmap_size, size_t num_buckets,
            bool verbose)
{
  void *db;
  if (verbose)
    printf ("Creating database %s\n", dbfile);
#ifdef HAVE_KYOTOCABINET
  char mmap_str[32];
  char buckets_str[32];
  char db_str[512] = "";
  if (strlen (dbfile) > (sizeof (db_str)-sizeof (buckets_str)-sizeof (mmap_str))
    error (EXIT_FAILURE, errno, "Filename too long");
  strncat (db_str, dbfile, strlen (dbfile));
  snprintf (mmap_str, sizeof(mmap_str), "#msiz=%lu", mmap_size);
  if (strlen (dbfile) + strlen (mmap_str) < sizeof (db_str))
    strncat (db_str, mmap_str, 32);
  else
    error (EXIT_FAILURE, errno, "Filename too long");
  snprintf (buckets_str, sizeof(buckets_str), "#bnum=%lu", num_buckets);
  if (strlen (dbfile) + strlen (mmap_str) < sizeof (db_str))
    strncat (db_str, buckets_str, 32);
  else
    error (EXIT_FAILURE, errno, "Filename too long");
  db = zdb_open (db_str, ZDB_CREATOR, false);
#else
  db = zdb_open (dbfile, ZDB_CREATOR, false);
  GDBM_FILE database = (GDBM_FILE)db;
  gdbm_setopt (database, GDBM_SETMAXMAPSIZE, &mmap_size, sizeof (size_t));
  gdbm_setopt (database, GDBM_SETCACHESIZE, &num_buckets, sizeof (size_t));
#endif
  return db;
}

void *
zdb_open (char *dbfile, int mode, bool verbose)
{
  void *db;
  if (verbose)
    printf ("Opening database %s\n", dbfile);
#ifdef HAVE_KYOTOCABINET
  KCDB *database;
  int32_t res;
  database = kcdbnew ();
  switch (mode)
    {
    case ZDB_READER:
      res = kcdbopen (database, dbfile, KCOREADER);
      break;
    case ZDB_WRITER:
      res = kcdbopen (database, dbfile, KCOWRITER);
      break;
    case ZDB_CREATOR:
      res = kcdbopen (database, dbfile, KCOWRITER | KCOCREATE);
      break;
    }
  if (!res)
    error (EXIT_FAILURE, errno,
           "Failed to open database %s", dbfile);
  db = database;
#else
  GDBM_FILE database;
  switch (mode)
    {
    case ZDB_READER:
      database = gdbm_open (dbfile, 512, GDBM_READER,
                 S_IRUSR | S_IWUSR | S_IRGRP | S_IWGRP, NULL);
      break;
    case ZDB_WRITER:
      database = gdbm_open (dbfile, 512, GDBM_WRITER,
                 S_IRUSR | S_IWUSR | S_IRGRP | S_IWGRP, NULL);
      break;
    case ZDB_CREATOR:
      database = gdbm_open (dbfile, 512, GDBM_WRCREAT,
                 S_IRUSR | S_IWUSR | S_IRGRP | S_IWGRP, NULL);
      break;
    }
  if (!database)
    error (EXIT_FAILURE, errno,
           "Failed to open database %s", dbfile);
  db = (void *)database;
#endif
  return db;
}

int
zdb_close (void *db, bool verbose)
{
  if (verbose)
    printf ("Closing database\n");
#ifdef HAVE_KYOTOCABINET
  KCDB *database = (KCDB *)db;
  if (!kcdbclose (database))
    return 1;
  kcdbdel (database);
#else
  GDBM_FILE database = (GDBM_FILE)db;
  gdbm_close (database);
#endif
  return 0;
}

int
zdb_store (void *db, char *key_str, size_t key_size, char *value_str,
           size_t value_size, bool verbose)
{
  if (verbose)
    printf ("Storing %s|%s                             \r", key_str, value_str);
#ifdef HAVE_KYOTOCABINET
  KCDB *database = (KCDB *)db;
  if (!kcdbset (database, key_str, key_size, value_str, value_size))
    return 1;
#else
  GDBM_FILE database = (GDBM_FILE)db;
  datum key, value;
  value.dptr = value_str;
  value.dsize = value_size;
  key.dptr = key_str;
  key.dsize = key_size;
  if (gdbm_store (database, key, value, GDBM_REPLACE))
    return 1;
#endif
  return 0;
}

void
printrec (char *key, char *value, char *delim)
{
  if (delim)
    printf ("%s%s%s\n", key, delim, value);
  else
    printf ("%s\n", value);
}

int
zdb_fetch (void *db, char *key_str, size_t key_size, char *delim)
{
  char *value_str;
#ifdef HAVE_KYOTOCABINET
  KCDB *database = (KCDB *)db;
  size_t value_size;
  value_str = kcdbget (database, key_str, key_size, &value_size);
#else
  GDBM_FILE database = (GDBM_FILE)db;
  datum key, value;
  key.dptr = key_str;
  key.dsize = key_size;
  value = gdbm_fetch (database, key);
  if (value.dptr == NULL)
    return 1;
  value_str = strndup (value.dptr, value.dsize);
  free (value.dptr);
#endif
  if (!value_str)
    return 1;
  printrec (key_str, value_str, delim);
#ifdef HAVE_KYOTOCABINET
  kcfree (value_str);
#else
  free (value_str);
#endif
  return 0;
}

int
zdb_fetchall (void *db, char *delim)
{
  char *key_str;
#ifdef HAVE_KYOTOCABINET
  KCDB *database = (KCDB *)db;
  KCCUR *cur;
  const char *value_str;
  size_t key_size, value_size;
  cur = kcdbcursor (database);
  kccurjump (cur);
  while ((key_str = kccurget(cur, &key_size, &value_str, &value_size, 1)) != NULL)
    {
      printrec (key_str, (char *) value_str, delim);
      kcfree (key_str);
    }
  kccurdel (cur);
#else
  char *value_str;
  datum key, nextkey, value;
  GDBM_FILE database = (GDBM_FILE)db;
  key = gdbm_firstkey (database);
  while (key.dptr)
    {
      value = gdbm_fetch (database, key);
      if (value.dptr == NULL)
        error (0, errno, "Key with non-existent value: %s", key.dptr);
      value_str = strndup (value.dptr, value.dsize);
      key_str = strndup (key.dptr, key.dsize);
      printrec (key_str, value_str, delim);
      free (value.dptr);
      free (key_str);
      free (value_str);
      nextkey = gdbm_nextkey (database, key);
      free (key.dptr);
      key = nextkey;
    }
#endif
  return (0);
}

int
zdb_remove (void *db, char *key_str, size_t key_size, bool verbose)
{
  if (verbose)
    printf ("Removing %s\r", key_str);
#ifdef HAVE_KYOTOCABINET
  KCDB *database = (KCDB *)db;
  if (!kcdbremove (database, key_str, key_size))
    return 1;
#else
  GDBM_FILE database = (GDBM_FILE)db;
  datum key;
  key.dptr = key_str;
  key.dsize = key_size;
  if (gdbm_delete (database, key))
    return 1;
#endif
  return 0;
}

int
zdb_reorganize (void *db)
{
#ifdef HAVE_KYOTOCABINET
  return 0;
#else
  GDBM_FILE database = (GDBM_FILE)db;
  if (gdbm_reorganize (database))
    return 1;
  return 0;
#endif
}
