/* zdbs.c

   Copyright © 2011, 2012, 2013, 2015 Brandon Invergo <brandon@invergo.net>

   This file is part of zeptodb

   zeptodb is free software: you can redistribute it and/or modify
   it under the terms of the GNU General Public License as published by
   the Free Software Foundation, either version 3 of the License, or
   (at your option) any later version.

   zeptodb is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.

   You should have received a copy of the GNU General Public License
   along with zeptodb.  If not, see <http://www.gnu.org/licenses/>.
*/

#define _GNU_SOURCE

#include <config.h>

#include <string.h>
#include "error.h"
#include "argp.h"
#include <signal.h>
#include <stdbool.h>

#include "zdb.h"

char *program_name = "zdbs";

const char *argp_program_version = PACKAGE_STRING;
const char *argp_program_bug_address = PACKAGE_BUGREPORT;
void *database;

static char doc[] =
  "zdbs -- a tool for storing records in a DBM database\v"
  "If an input file is provided, lines are read from it as new records, "
  "otherwise records are added from stdin. Records are expected in the "
  "format KEY|VALUE.";

static char args_doc[] = "DATABASE";

static struct argp_option options[] = {
  {"input", 'i', "FILE", 0, "Read new records from a file"},
  {"delim", 'd', "CHAR", 0,
   "Character used to separate keys from values (default '|')"},
  {"verbose", 'v', 0, 0, "Print extra information."},
  {0}
};

struct arguments
{
  char *args[1];
  char *input_file;
  const char *delim;
  bool verbose;
};

static error_t
parse_opt (int key, char *arg, struct argp_state *state)
{
  struct arguments *arguments = state->input;

  switch (key)
    {
    case 'i':
      arguments->input_file = arg;
      break;

    case 'd':
      if (strlen (arg) > 1)
        argp_usage (state);
      arguments->delim = arg;
      break;

    case 'v':
      arguments->verbose = true;
      break;

    case ARGP_KEY_ARG:
      if (state->arg_num >= 1)
        argp_usage (state);
      arguments->args[state->arg_num] = arg;
      break;

    case ARGP_KEY_END:
      if (state->arg_num == 0)
        argp_usage (state);
      break;

    default:
      return ARGP_ERR_UNKNOWN;
    }
  return 0;
}

static struct argp argp = { options, parse_opt, args_doc, doc };

int
addrecords (void *db, FILE *input, const char *delim, bool verbose)
{
  size_t linelen = 512;
  char *lineptr = (char *) malloc (linelen);
  char *linecpy;
  char *key_str, *value_str;
  size_t line_size, key_size, value_size;
  long int count = 1;

  /* add each line of the input to the database */
  while (getline (&lineptr, &linelen, input) != EOF)
    {
      line_size = strlen (lineptr);
      if (lineptr[line_size - 1] == '\n')
        line_size -= 1;
      if (line_size <= 0)
        error (0, errno, "Empty record");
      linecpy = (char *) malloc (sizeof (char) * (line_size+1));
      if (!linecpy)
        {
          free (lineptr);
          error (EXIT_FAILURE, errno, "Virtual memory exhausted");
        }
      strncpy (linecpy, lineptr, sizeof (char) * (line_size));
      linecpy[line_size] = '\0';
      key_str = strtok (linecpy, delim);
      if (!key_str)
        {
          error (0, errno, "Invalid record format (key%svalue)", delim);
          continue;
        }
      value_str = strtok (NULL, delim);
      if (!value_str)
        {
          error (0, errno, "Invalid record format (key%svalue)", delim);
          continue;
        }
      value_size = strlen (value_str);
      key_size = strlen (key_str);
      if (verbose)
        printf ("%ld: ", count);
      if (zdb_store (db, key_str, key_size, value_str, value_size, verbose))
        error (0, errno, "Could not store record in database: %s", key_str);
      count++;
      free (linecpy);
    }
  free (lineptr);
  return (0);
}

void
termination_handler (int signum)
{
  printf ("Interrupt caught, closing database\n");
  if (zdb_close (database, false))
    error (EXIT_FAILURE, errno, "Failed to close database");
  else
    exit (EXIT_SUCCESS);
}

int
main (int argc, char **argv)
{
  FILE *input;
  struct arguments arguments;
  int result;

  arguments.input_file = "-";
  arguments.delim = "|";
  arguments.verbose = false;

  argp_parse (&argp, argc, argv, 0, 0, &arguments);

  if (signal (SIGINT, termination_handler) == SIG_IGN)
    signal (SIGINT, SIG_IGN);
  if (signal (SIGHUP, termination_handler) == SIG_IGN)
    signal (SIGHUP, SIG_IGN);
  if (signal (SIGTERM, termination_handler) == SIG_IGN)
    signal (SIGTERM, SIG_IGN);

  /* open the database */
  database = zdb_open (arguments.args[0], ZDB_WRITER,  arguments.verbose);
  if (!database)
    error (EXIT_FAILURE, errno,
           "Failed to open database %s", arguments.args[0]);

  /* if an input file was provided, use it, otherwise use stdin */
  if (strcmp (arguments.input_file, "-") == 0)
    input = stdin;
  else
    input = fopen (arguments.input_file, "r");
  if (!input)
    error (EXIT_FAILURE, errno,
           "Failed to open input file %s", arguments.input_file);
  result = addrecords (database, input, arguments.delim, arguments.verbose);

  fclose (input);
  if (zdb_close (database, arguments.verbose))
    error (EXIT_FAILURE, errno,
           "Failed to close database %s", arguments.args[0]);
  if (result != 0)
    exit (EXIT_FAILURE);
  exit (EXIT_SUCCESS);
}
