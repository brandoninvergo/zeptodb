/* zdb.h

   Copyright © 2013 Brandon Invergo <brandon@invergo.net>

   This file is part of zeptodb

   zeptodb is free software: you can redistribute it and/or modify
   it under the terms of the GNU General Public License as published by
   the Free Software Foundation, either version 3 of the License, or
   (at your option) any later version.

   zeptodb is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.

   You should have received a copy of the GNU General Public License
   along with zeptodb.  If not, see <http://www.gnu.org/licenses/>.
*/

#ifndef ZDB_H
#define ZDB_H
#define _GNU_SOURCE

#include <config.h>

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include "error.h"
#include <errno.h>
#include <sys/stat.h>
#include <stdbool.h>

#ifdef HAVE_KYOTOCABINET
#include <kclangc.h>
#else
#include <gdbm.h>
#endif

#define ZDB_READER 0
#define ZDB_WRITER 1
#define ZDB_CREATOR 2

void *zdb_create (char *, unsigned long int, unsigned long int, bool);
void *zdb_open (char *, int, bool);
int zdb_close (void *, bool);
int zdb_store (void *, char *, size_t, char *, size_t, bool);
void printrec (char *, char *, char *);
int zdb_fetch (void *, char *, size_t, char *);
int zdb_fetchall (void *, char *);
int zdb_remove (void *, char *, size_t, bool);
int zdb_reorganize (void *);

#endif
