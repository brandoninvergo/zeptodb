/* zdbf.c

   Copyright © 2011, 2012, 2013, 2015 Brandon Invergo <brandon@invergo.net>

   This file is part of zeptodb

   zeptodb is free software: you can redistribute it and/or modify
   it under the terms of the GNU General Public License as published by
   the Free Software Foundation, either version 3 of the License, or
   (at your option) any later version.

   zeptodb is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.

   You should have received a copy of the GNU General Public License
   along with zeptodb.  If not, see <http://www.gnu.org/licenses/>.
*/

#define _GNU_SOURCE

#include <config.h>

#include <string.h>
#include "error.h"
#include "argp.h"
#include <signal.h>
#include <stdbool.h>

#include "zdb.h"

char *program_name = "zdbf";

const char *argp_program_version = PACKAGE_STRING;
const char *argp_program_bug_address = PACKAGE_BUGREPORT;
void *database;

static char doc[] =
  "zdbf -- a tool for fetching records from a DBM database\v"
  "If an input file is provided, lines are read from it as individual queries, "
  "otherwise queries are taken from stdin.";

static char args_doc[] = "DATABASE";

static struct argp_option options[] = {
  {"input", 'i', "FILE", 0, "Read queries from a file"},
  {"all", 'a', 0, 0, "Fetch all the records in the database"},
  {"delim", 'd', "CHAR", 0, "Character to print between keys and values."
   "Keys will not be printed if left unspecified."},
  {"verbose", 'v', 0, 0, "Print extra information."},
  {0}
};

struct arguments
{
  char *args[1];
  char *input_file;
  bool all;
  char *delim;
  bool verbose;
};

static error_t
parse_opt (int key, char *arg, struct argp_state *state)
{
  struct arguments *arguments = state->input;

  switch (key)
    {
    case 'i':
      arguments->input_file = arg;
      break;

    case 'a':
      arguments->all = true;
      break;

    case 'd':
      if (strlen (arg) > 1)
        argp_usage (state);
      arguments->delim = arg;
      break;

    case 'v':
      arguments->verbose = true;
      break;

    case ARGP_KEY_ARG:
      if (state->arg_num >= 1)
        argp_usage (state);
      arguments->args[state->arg_num] = arg;
      break;

    case ARGP_KEY_END:
      if (state->arg_num == 0)
        argp_usage (state);
      break;

    default:
      return ARGP_ERR_UNKNOWN;
    }
  return 0;
}

static struct argp argp = { options, parse_opt, args_doc, doc };

int
fetchqueries (void *db, FILE * input, char *delim)
{
  /*
   * This function fetches records from the database from queries
   * in a file object.
   */

  size_t linelen = 512;
  char *lineptr = (char *) malloc (linelen);
  char *key_str;
  size_t key_size;

  /* query the database for each line of input */
  while (getline (&lineptr, &linelen, input) != EOF)
    {
      key_size = strlen (lineptr);
      if (lineptr[key_size - 1] == '\n')
        key_size -= 1;
      if (key_size <= 0)
        error (0, errno, "Empty key value");
      key_str = (char *) malloc (sizeof (char) * (key_size+1));
      if (!key_str)
        {
          free (lineptr);
          error (EXIT_FAILURE, errno, "Virtual memory exhausted");
        }
      /* get rid of the newline */
      strncpy (key_str, lineptr, sizeof (char) * (key_size));
      key_str[key_size] = '\0';
      if (zdb_fetch (db, key_str, key_size, delim))
        error (0, errno, "Key does not exist in the database: %s", key_str);
      free (key_str);
    }
  free (lineptr);
  return (0);
}

void
termination_handler (int signum)
{
  printf ("Interrupt caught, closing database\n");
  if (zdb_close (database, false))
    error (EXIT_FAILURE, errno, "Failed to close database");
  else
    exit (EXIT_SUCCESS);
}

int
main (int argc, char **argv)
{
  FILE *input;
  struct arguments arguments;
  arguments.input_file = "-";
  arguments.all = false;
  arguments.verbose = false;
  arguments.delim = NULL;

  argp_parse (&argp, argc, argv, 0, 0, &arguments);

  if (signal (SIGINT, termination_handler) == SIG_IGN)
    signal (SIGINT, SIG_IGN);
  if (signal (SIGHUP, termination_handler) == SIG_IGN)
    signal (SIGHUP, SIG_IGN);
  if (signal (SIGTERM, termination_handler) == SIG_IGN)
    signal (SIGTERM, SIG_IGN);

  /* open the database */
  database = zdb_open (arguments.args[0], ZDB_READER, arguments.verbose);
  if (!database)
    error (EXIT_FAILURE, errno,
           "Failed to open database %s", arguments.args[0]);

  /* if the "all" argument was passed, output all the records */
  /* and exit */
  if (arguments.all != 0)
    {
      zdb_fetchall (database, arguments.delim);
      zdb_close (database, arguments.verbose);
      exit (EXIT_SUCCESS);
    }

  /* if an input file was provided, use it, otherwise use stdin */
  if (strcmp (arguments.input_file, "-") == 0)
    input = stdin;
  else
    input = fopen (arguments.input_file, "r");
  if (!input)
    error (EXIT_FAILURE, errno,
           "Failed to open input file %s", arguments.input_file);
  fetchqueries (database, input, arguments.delim);

  fclose (input);

  if (zdb_close (database, arguments.verbose))
    error (EXIT_FAILURE, errno,
           "Failed to close database %s", arguments.args[0]);
  exit (EXIT_SUCCESS);
}
