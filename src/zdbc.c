/* zdbc.c

   Copyright © 2013, 2015 Brandon Invergo <brandon@invergo.net>

   This file is part of zeptodb

   zeptodb is free software: you can redistribute it and/or modify
   it under the terms of the GNU General Public License as published by
   the Free Software Foundation, either version 3 of the License, or
   (at your option) any later version.

   zeptodb is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.

   You should have received a copy of the GNU General Public License
   along with zeptodb.  If not, see <http://www.gnu.org/licenses/>.
*/

#define _GNU_SOURCE

#include <config.h>

#include <string.h>
#include "error.h"
#include "argp.h"
#include <signal.h>
#include <stdbool.h>
#include <stdlib.h>

#include "zdb.h"

char *program_name = "zdbc";

const char *argp_program_version = PACKAGE_STRING;
const char *argp_program_bug_address = PACKAGE_BUGREPORT;
void *database;

static char doc[] =
  "zdbc -- a tool for creating a DBM database";

static char args_doc[] = "DATABASE";

static struct argp_option options[] = {
  {"mmap-size", 'm', "NUM", 0, "The size (in bytes) of the memory-mapped region"
   " to use (default=1024)"},
  {"num-buckets", 'b', "NUM", 0, "The number of buckets to use (default=100)"},
  {"verbose", 'v', 0, 0, "Print extra information."},
  {0}
};

struct arguments
{
  char *args[1];
  size_t mmap_size;
  size_t num_buckets;
  const char *delim;
  bool verbose;
};

static error_t
parse_opt (int key, char *arg, struct argp_state *state)
{
  struct arguments *arguments = state->input;
  unsigned long int size;
  switch (key)
    {
    case 'm':
      size = arg ? strtoul (arg, NULL, 10) : 1024;
      if (size > sizeof(size_t) || size <= 0)
        {
          arguments->mmap_size = sizeof(size_t);
        }
      else
        {
          arguments->mmap_size = (size_t)size;
        }
      break;

    case 'b':
      size = arg ? strtoul (arg, NULL, 10) : 100;
      if (size > sizeof(size_t) || size <= 0)
        {
          arguments->num_buckets = sizeof(size_t);
        }
      else
        {
          arguments->num_buckets = (size_t)size;
        }
      break;

    case 'v':
      arguments->verbose = true;
      break;

    case ARGP_KEY_ARG:
      if (state->arg_num >= 1)
        argp_usage (state);
      arguments->args[state->arg_num] = arg;
      break;

    case ARGP_KEY_END:
      if (state->arg_num == 0)
        argp_usage (state);
      break;

    default:
      return ARGP_ERR_UNKNOWN;
    }
  return 0;
}

static struct argp argp = { options, parse_opt, args_doc, doc };

void
termination_handler (int signum)
{
  printf ("Interrupt caught, closing database\n");
  if (zdb_close (database, false))
    error (EXIT_FAILURE, errno, "Failed to close database");
  else
    exit (EXIT_SUCCESS);
}

int
main (int argc, char **argv)
{
  struct arguments arguments;

  arguments.verbose = false;
  arguments.mmap_size = 1024;
  arguments.num_buckets = 10;

  argp_parse (&argp, argc, argv, 0, 0, &arguments);

  if (signal (SIGINT, termination_handler) == SIG_IGN)
    signal (SIGINT, SIG_IGN);
  if (signal (SIGHUP, termination_handler) == SIG_IGN)
    signal (SIGHUP, SIG_IGN);
  if (signal (SIGTERM, termination_handler) == SIG_IGN)
    signal (SIGTERM, SIG_IGN);

  /* open the database */
  database = zdb_create (arguments.args[0], arguments.mmap_size,
                         arguments.num_buckets,  arguments.verbose);
  if (!database)
    error (EXIT_FAILURE, errno,
           "Failed to create database %s", arguments.args[0]);

  if (zdb_close (database, arguments.verbose))
    error (EXIT_FAILURE, errno,
           "Failed to close database %s", arguments.args[0]);
  exit (EXIT_SUCCESS);
}
